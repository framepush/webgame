﻿using UnityEngine;
using UnityEngine.AI;
using UnityStandardAssets.Characters.ThirdPerson;

[RequireComponent(typeof(NavMeshAgent))]
[RequireComponent(typeof(ThirdPersonCharacter))]
public class GroundClickNav : MonoBehaviour
{
    public Camera cam;

    private LayerMask _mask;
    private NavMeshAgent _agent;
    private Vector3 _target;
    private ThirdPersonCharacter _character;

    private void Awake()
    {
        _mask = LayerMask.GetMask("Ground");
        _agent = GetComponent<NavMeshAgent>();
        _character = GetComponent<ThirdPersonCharacter>();
    }

    private void Start()
    {
        if (!cam)
        {
            cam = Camera.main;
        }

        _agent.updateRotation = false;
        _agent.updatePosition = true;
    }

    private void Update()
    {
        if (Input.GetMouseButtonUp(0))
        {
            RaycastHit hitInfo;
            var hit = Physics.Raycast(cam.ScreenPointToRay(Input.mousePosition), out hitInfo, 100, _mask);
            if (hit)
            {
                _target = hitInfo.point;
                _agent.SetDestination(_target);
            }
        }

        if (_agent.remainingDistance > _agent.stoppingDistance)
        {
            _character.Move(_agent.desiredVelocity, false, false);
        }
        else
        {
            _character.Move(Vector3.zero, false, false);
        }
    }
}
