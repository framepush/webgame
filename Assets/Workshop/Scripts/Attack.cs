﻿using UnityEngine;

public class Attack : MonoBehaviour
{
    public GameObject prefab;

    public Transform spawn;

    public float speed = 10;

    void Update()
    {
        if (Input.GetMouseButtonDown(1))
        {
            MakeBall();
        }
    }

    private void MakeBall()
    {
        GameObject go = Instantiate(prefab, spawn.position, spawn.rotation);
        Rigidbody rb = go.GetComponent<Rigidbody>();
        rb.velocity = spawn.forward * speed;
    }
}
