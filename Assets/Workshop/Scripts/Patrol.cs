﻿using System.Collections;
using UnityEngine;
using UnityStandardAssets.Characters.ThirdPerson;

[RequireComponent(typeof(AICharacterControl))]
public class Patrol : MonoBehaviour
{
    public float delay = 15;
    public Transform[] locations;

    private AICharacterControl _character;
    private int _current;

    private void Awake()
    {
        _character = GetComponent<AICharacterControl>();
    }

    private void Start()
    {
        StartCoroutine(PatrolArea());
    }

    IEnumerator PatrolArea()
    {
        while (true)
        {
            _character.target = locations[_current];
            ++_current;
            if (_current >= locations.Length)
            {
                _current = 0;
            }
            yield return new WaitForSeconds(delay);
        }
    }
}
